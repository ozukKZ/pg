spawn(function()
game.StarterGui:SetCore("SendNotification", {
Title = "Checkpoint!3";
Text = "7 save,8 tp",
Icon = "rbxassetid://5472203252";
Duration = 9;
})
local TPOP3 = Instance.new("ScreenGui")
local main3 = Instance.new("Frame")
local PT3 = Instance.new("TextLabel")
local TPTO3 = Instance.new("Frame")
local PL3 = Instance.new("TextLabel")


TPOP3.Name = "TPOP3"
TPOP3.Parent = game.CoreGui

main3.Name = "main3"
main3.Parent = TPOP3
main3.BackgroundColor3 = Color3.new(0.384314, 0.384314, 0.384314)
main3.Position = UDim2.new(0.997280121, 0, 0.517114937, 0)
main3.Size = UDim2.new(0, 140, 0, 40)

PT3.Name = "PT3"
PT3.Parent = main3
PT3.BackgroundColor3 = Color3.new(1, 1, 1)
PT3.BackgroundTransparency = 1
PT3.Position = UDim2.new(-0.00198434386, 0, 0.00943762809, 0)
PT3.Size = UDim2.new(0, 140, 0, 40)
PT3.Font = Enum.Font.SourceSansSemibold
PT3.Text = "Position 3 Has Been Saved!"
PT3.TextColor3 = Color3.new(0, 255, 255)
PT3.TextSize = 15
PT3.TextWrapped = true

TPTO3.Name = "TPTO3"
TPTO3.Parent = TPOP3
TPTO3.BackgroundColor3 = Color3.new(0.384314, 0.384314, 0.384314)
TPTO3.Position = UDim2.new(0.997280121, 0, 0.57823962, 0)
TPTO3.Size = UDim2.new(0, 140, 0, 40)

PL3.Name = "PL3"
PL3.Parent = TPTO3
PL3.BackgroundColor3 = Color3.new(1, 1, 1)
PL3.BackgroundTransparency = 1
PL3.Position = UDim2.new(-0.00198434386, 0, 0.00943762809, 0)
PL3.Size = UDim2.new(0, 140, 0, 40)
PL3.Font = Enum.Font.SourceSansSemibold
PL3.Text = "Position 3 Has Been Loaded!<3"
PL3.TextColor3 = Color3.new(1, 0.666667, 0)
PL3.TextSize = 16
PL3.TextWrapped = true


PT3.Text = "Save7, Load8"

wait(1)

main3:TweenPosition(UDim2.new(0.911181152, 0, 0.517114937, 0), "Out")

wait(2)

main3:TweenPosition(UDim2.new(0.99728012, 0, 0.517114937, 0), "In")

wait(1)

PT3.Text = "Position 3 Has Been Saved!"


function onKeyPress3(inputObject, gameProcessedEvent3)
if inputObject.KeyCode == Enum.KeyCode.KeypadSeven then --KeypadOne
LPos3 = game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame
main3:TweenPosition(UDim2.new(0.911181152, 0, 0.517114937, 0), "Out")
wait(2)
main3:TweenPosition(UDim2.new(0.99728012, 0, 0.517114937, 0), "In")
end
end
game:GetService("UserInputService").InputBegan:connect(onKeyPress3)

function onKeyPress3(inputObject, gameProcessedEvent3)
if inputObject.KeyCode == Enum.KeyCode.KeypadEight then --KeypadTwo
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = LPos3----------------
TPTO3:TweenPosition(UDim2.new(0.911221445, 0, 0.57823962, 0), "Out")
wait(2)
TPTO3:TweenPosition(UDim2.new(0.99728012, 0, 0.57823962, 0), "In")
end
end
game:GetService("UserInputService").InputBegan:connect(onKeyPress3)
--
end)
