if SP_LOADED and not _G.SP_DEBUG == true then
    error("Spectateplayer is already running!",0)
    return
end
pcall(function() getgenv().SP_LOADED  = true end)--
--
game.StarterGui:SetCore("SendNotification", {
Title = "Simple Spectate Player";
Text = "Numpad 0 to HideGUI",
Icon = "rbxassetid://10678268125";
Duration = 10;
})
--
-- Services
local Players = game:GetService("Players")
local UserInputService = game:GetService("UserInputService")

-- Create GUI for Spectating
local screenGui = Instance.new("ScreenGui", game.Players.LocalPlayer.PlayerGui)
screenGui.Name = "SpectateGui"
screenGui.ResetOnSpawn = false -- Prevent resetting on respawn

-- Create main frame for GUI
local mainFrame = Instance.new("Frame", screenGui)
mainFrame.Size = UDim2.new(0, 300, 0, 300)  -- Increased height for longer player list
mainFrame.Position = UDim2.new(0.5, 300, 0.5, -400)
mainFrame.BackgroundColor3 = Color3.new(0.1, 0.1, 0.1)
mainFrame.Draggable = true
mainFrame.Active = true

-- Create buttons for switching players and spectate toggle
local previousButton = Instance.new("TextButton", mainFrame)
previousButton.Text = "<"
previousButton.Size = UDim2.new(0, 50, 0, 50)
previousButton.Position = UDim2.new(0, 10, 0, 25)

local nextButton = Instance.new("TextButton", mainFrame)
nextButton.Text = ">"
nextButton.Size = UDim2.new(0, 50, 0, 50)
nextButton.Position = UDim2.new(1, -60, 0, 25)

local toggleSpectateButton = Instance.new("TextButton", mainFrame)
toggleSpectateButton.Text = "Start Spectate"
toggleSpectateButton.Size = UDim2.new(0, 150, 0, 50)
toggleSpectateButton.Position = UDim2.new(0.5, -75, 0, 25)

-- Create label for player name
local playerNameLabel = Instance.new("TextLabel", mainFrame)
playerNameLabel.Size = UDim2.new(0, 300, 0, 25)
playerNameLabel.Position = UDim2.new(0, 0, 0, 80)
playerNameLabel.BackgroundTransparency = 1
playerNameLabel.Text = "Player: "
playerNameLabel.TextSize = 18
playerNameLabel.TextColor3 = Color3.new(1, 1, 1)

-- Display name toggle
local displayNameToggle = Instance.new("TextButton", mainFrame)
displayNameToggle.Text = "DisplayName: ON"
displayNameToggle.Size = UDim2.new(0, 150, 0, 25)
displayNameToggle.Position = UDim2.new(0.5, -75, 0, 110)

-- Create teleport button
local teleportButton = Instance.new("TextButton", mainFrame)
teleportButton.Text = "Teleport to Player"
teleportButton.Size = UDim2.new(0, 150, 0, 25)
teleportButton.Position = UDim2.new(0.5, -75, 0, 140)

-- Player list scrolling frame (longer size)
local playerListFrame = Instance.new("ScrollingFrame", mainFrame)
playerListFrame.Size = UDim2.new(0, 280, 0, 150)  -- Increased height for longer list
playerListFrame.Position = UDim2.new(0, 10, 0, 170)
playerListFrame.ScrollBarThickness = 8
playerListFrame.BackgroundColor3 = Color3.new(0.15, 0.15, 0.15)

-- Variables
local currentPlayerIndex = 1
local isSpectating = false
local useDisplayName = true
local players = Players:GetPlayers()

-- Function to instantly switch the camera to the target player
local function switchCameraToPlayer(targetPlayer)
    if targetPlayer and targetPlayer.Character then
        game.Workspace.CurrentCamera.CameraSubject = targetPlayer.Character:FindFirstChild("Humanoid")
    end
end

-- Function to update the GUI with player info
local function updateSpectateInfo()
    local targetPlayer = players[currentPlayerIndex]
    if useDisplayName then
        playerNameLabel.Text = "Player: " .. targetPlayer.DisplayName
    else
        playerNameLabel.Text = "Player: " .. targetPlayer.Name
    end
    if isSpectating then
        switchCameraToPlayer(targetPlayer) -- Instant switch
    end
end

-- Function to start spectating
local function startSpectating()
    local targetPlayer = players[currentPlayerIndex]
    switchCameraToPlayer(targetPlayer)
    toggleSpectateButton.Text = "Stop Spectate"
    isSpectating = true
    updateSpectateInfo()
end

-- Function to stop spectating
local function stopSpectating()
    game.Workspace.CurrentCamera.CameraSubject = game.Players.LocalPlayer.Character:FindFirstChild("Humanoid")
    toggleSpectateButton.Text = "Start Spectate"
    isSpectating = false
end

-- Function to update the player list
local function updatePlayerList()
    playerListFrame:ClearAllChildren() -- Clear existing buttons
    players = Players:GetPlayers() -- Refresh the players list
    for i, player in ipairs(players) do
        local playerButton = Instance.new("TextButton", playerListFrame)
        playerButton.Text = useDisplayName and player.DisplayName or player.Name
        playerButton.Size = UDim2.new(0, 260, 0, 30)
        playerButton.Position = UDim2.new(0, 10, 0, (i - 1) * 35)
        playerButton.MouseButton1Click:Connect(function()
            currentPlayerIndex = i
            updateSpectateInfo() -- Update spectating info immediately
            if isSpectating then
                switchCameraToPlayer(players[currentPlayerIndex]) -- Ensure camera switch while spectating
            end
        end)
    end
    playerListFrame.CanvasSize = UDim2.new(0, 0, 0, #players * 35) -- Adjust scroll size
end

-- Button actions
previousButton.MouseButton1Click:Connect(function()
    currentPlayerIndex = (currentPlayerIndex - 1 - 1) % #players + 1
    updateSpectateInfo()
    if isSpectating then
        switchCameraToPlayer(players[currentPlayerIndex])
    end
end)

nextButton.MouseButton1Click:Connect(function()
    currentPlayerIndex = (currentPlayerIndex % #players) + 1
    updateSpectateInfo()
    if isSpectating then
        switchCameraToPlayer(players[currentPlayerIndex])
    end
end)

toggleSpectateButton.MouseButton1Click:Connect(function()
    if isSpectating then
        stopSpectating()
    else
        startSpectating()
    end
end)

displayNameToggle.MouseButton1Click:Connect(function()
    useDisplayName = not useDisplayName
    displayNameToggle.Text = useDisplayName and "DisplayName: ON" or "DisplayName: OFF"
    updatePlayerList()
    updateSpectateInfo()
end)

teleportButton.MouseButton1Click:Connect(function()
    local targetPlayer = players[currentPlayerIndex]
    if targetPlayer and targetPlayer.Character then
        game.Players.LocalPlayer.Character:SetPrimaryPartCFrame(targetPlayer.Character:GetPrimaryPartCFrame())
    end
end)

-- Toggle UI visibility with Numpad 0
UserInputService.InputBegan:Connect(function(input)
    if input.KeyCode == Enum.KeyCode.KeypadZero then
        screenGui.Enabled = not screenGui.Enabled
    end
end)

-- Update player list when players join or leave
Players.PlayerAdded:Connect(function()
    updatePlayerList() -- Refresh when a player joins
end)

Players.PlayerRemoving:Connect(function()
    updatePlayerList() -- Refresh when a player leaves
end)

-- Initial update of player list and spectate info
updatePlayerList()
updateSpectateInfo()
