game.StarterGui:SetCore("SendNotification", {
Title = "how many checkpoints?";
Text = "PRESS - to hide this UI",
Icon = "rbxassetid://9633516381";
Duration = 30;
})
local ScreenGui = Instance.new("ScreenGui")
local Frame = Instance.new("Frame")
local Checkpoints1 = Instance.new("TextButton")
local Checkpoints2 = Instance.new("TextButton")
local Frame_2 = Instance.new("Frame")
local Checkpoints3 = Instance.new("TextButton")
--Properties:

ScreenGui.Parent = game.CoreGui
ScreenGui.ZIndexBehavior = Enum.ZIndexBehavior.Sibling

Frame.Parent = ScreenGui
Frame.BackgroundColor3 = Color3.fromRGB(70, 70, 70)
Frame.BorderSizePixel = 0
Frame.Position = UDim2.new(0.6817941949, 0, 0.031096994, 0)
Frame.Size = UDim2.new(0, 170, 0, 89)
Frame.Draggable = true 
Frame.Active = true 

Checkpoints1.Name = "Checkpoints1"
Checkpoints1.Parent = Frame
Checkpoints1.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Checkpoints1.BorderSizePixel = 0
Checkpoints1.Position = UDim2.new(0.0180065946, 0, 0.213617444, 0)
Checkpoints1.Size = UDim2.new(0, 70, 0, 22)
Checkpoints1.Style = Enum.ButtonStyle.RobloxRoundButton
Checkpoints1.Font = Enum.Font.SourceSans
Checkpoints1.Text = "Checkpoints1"
Checkpoints1.TextColor3 = Color3.fromRGB(255, 255, 255)
Checkpoints1.TextSize = 12.000
Checkpoints1.MouseButton1Down:connect(function()
loadstring(game:HttpGet("https://gitlab.com/ozukKZ/pg/-/raw/main/Checkpoint1.lua"))()
end)

Checkpoints2.Name = "Checkpoints2"
Checkpoints2.Parent = Frame
Checkpoints2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Checkpoints2.BorderSizePixel = 0
Checkpoints2.Position = UDim2.new(0.0180065946, 0, 0.513617444, 0)
Checkpoints2.Size = UDim2.new(0, 70, 0, 22)
Checkpoints2.Style = Enum.ButtonStyle.RobloxRoundButton
Checkpoints2.Font = Enum.Font.SourceSans
Checkpoints2.Text = "Checkpoints2"
Checkpoints2.TextColor3 = Color3.fromRGB(255, 255, 255)
Checkpoints2.TextSize = 12.000
Checkpoints2.MouseButton1Down:connect(function()
loadstring(game:HttpGet("https://gitlab.com/ozukKZ/pg/-/raw/main/Checkpoint2.lua"))()
end)

Frame_2.Parent = Frame
Frame_2.BackgroundColor3 = Color3.fromRGB(0, 255, 255)
Frame_2.BorderSizePixel = 0
Frame_2.Size = UDim2.new(0, 170, 0, 7)

Checkpoints3.Name = "Checkpoints3"
Checkpoints3.Parent = Frame
Checkpoints3.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Checkpoints3.BorderSizePixel = 0
Checkpoints3.Position = UDim2.new(0.544620593, 0, 0.213617444, 0)
Checkpoints3.Size = UDim2.new(0, 70, 0, 22)
Checkpoints3.Style = Enum.ButtonStyle.RobloxRoundButton
Checkpoints3.Font = Enum.Font.SourceSans
Checkpoints3.Text = "Checkpoints3"
Checkpoints3.TextColor3 = Color3.fromRGB(255, 255, 255)
Checkpoints3.TextSize = 12.000
Checkpoints3.MouseButton1Down:connect(function()
loadstring(game:HttpGet("https://gitlab.com/ozukKZ/pg/-/raw/main/checkpoint3.lua", true))()
end)

--scripts

local Frame = -- GUI
	game:GetService("Players").LocalPlayer:GetMouse().KeyDown:Connect(function(Key)
	if Key == "-" then
		Frame.Visible = not Frame.Visible
	end
end)
