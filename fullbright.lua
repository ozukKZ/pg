spawn(function()---+
game.StarterGui:SetCore("SendNotification", {
Title = "fullbright NUMPAD6";
Text = " NUMPAD6 to change lv of the light!",
Icon = "rbxassetid://10678266648";
Duration = 20;
})
local Light = game:GetService("Lighting")
local brightnessLevel = 1

local brightnessSettings = {
    {Ambient = Color3.new(0, 0, 0), ColorShift_Bottom = Color3.new(1, 1, 1), ColorShift_Top = Color3.new(1, 1, 1)},
    {Ambient = Color3.new(0.2, 0.2, 0.2), ColorShift_Bottom = Color3.new(0.8, 0.8, 0.8), ColorShift_Top = Color3.new(0.8, 0.8, 0.8)},
    {Ambient = Color3.new(0.4, 0.4, 0.4), ColorShift_Bottom = Color3.new(0.6, 0.6, 0.6), ColorShift_Top = Color3.new(0.6, 0.6, 0.6)},
    {Ambient = Color3.new(0.6, 0.6, 0.6), ColorShift_Bottom = Color3.new(0.4, 0.4, 0.4), ColorShift_Top = Color3.new(0.4, 0.4, 0.4)},
    {Ambient = Color3.new(1, 1, 1), ColorShift_Bottom = Color3.new(0, 0, 0), ColorShift_Top = Color3.new(0, 0, 0)},
}

local function applyBrightness(level)
    local settings = brightnessSettings[level]
    Light.Ambient = settings.Ambient
    Light.ColorShift_Bottom = settings.ColorShift_Bottom
    Light.ColorShift_Top = settings.ColorShift_Top
end

local function cycleBrightness()
    brightnessLevel = brightnessLevel % 5 + 1
    applyBrightness(brightnessLevel)
    game.StarterGui:SetCore("SendNotification", {
        Title = "Brightness Level";
        Text = "Level " .. brightnessLevel;
        Icon = "rbxassetid://120588722906399";
        Duration = 4;
    })
end

applyBrightness(brightnessLevel)

Light.LightingChanged:Connect(function()
    applyBrightness(brightnessLevel)
end)

local UserInputService = game:GetService("UserInputService")
UserInputService.InputBegan:Connect(function(input)
    if input.KeyCode == Enum.KeyCode.KeypadSix then
        cycleBrightness()
    end
end)
end)---+