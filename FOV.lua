spawn(function()

game.StarterGui:SetCore("SendNotification", {
Title = "Your FOV:";
Text = "WHY U NEED THIS",
Icon = "rbxassetid://18667948601";
Duration = 10;
})
-- Gui to Lua
-- Version: 3.2

-- Instances:

local FieldOfViewGUI = Instance.new("ScreenGui")
local Fovbox = Instance.new("TextBox")
local Fovset = Instance.new("TextButton")
local Camera = workspace:FindFirstChildOfClass("Camera")

--Properties:

FieldOfViewGUI.Name = "FieldOfViewGUI"
FieldOfViewGUI.Parent = game.Players.LocalPlayer:WaitForChild("PlayerGui")
FieldOfViewGUI.ZIndexBehavior = Enum.ZIndexBehavior.Sibling
FieldOfViewGUI.ResetOnSpawn = false

Fovbox.Name = "Fovbox"
Fovbox.Parent = FieldOfViewGUI
Fovbox.BackgroundColor3 = Color3.fromRGB(45, 45, 45)
Fovbox.BorderColor3 = Color3.fromRGB(64, 224, 208)
Fovbox.BorderSizePixel = 5
Fovbox.Position = UDim2.new(0.0154202003, 0, 0.9153121315, 0)
Fovbox.Size = UDim2.new(0, 70, 0, 50)
Fovbox.Font = Enum.Font.Highway
Fovbox.PlaceholderText = "FOV (Number only)"
Fovbox.Text = ""
Fovbox.TextColor3 = Color3.fromRGB(255, 255, 255)
Fovbox.TextScaled = true
Fovbox.TextSize = 14.000
Fovbox.TextStrokeTransparency = 0.000
Fovbox.TextWrapped = true

Fovset.Name = "Fovset"
Fovset.Parent = FieldOfViewGUI
Fovset.BackgroundColor3 = Color3.fromRGB(45, 45, 45)
Fovset.BorderColor3 = Color3.fromRGB(64, 224, 208)
Fovset.BorderSizePixel = 5
Fovset.Position = UDim2.new(0.078103313, 0, 0.9153121315, 0)
Fovset.Size = UDim2.new(0, 72, 0, 31)
Fovset.Font = Enum.Font.Highway
Fovset.Text = "Set FOV"
Fovset.TextColor3 = Color3.fromRGB(255, 255, 255)
Fovset.TextScaled = true
Fovset.TextSize = 14.000
Fovset.TextStrokeTransparency = -1.000
Fovset.TextWrapped = true

-- Scripts:

Fovset.MouseButton1Click:Connect(function()
	Camera.FieldOfView = Fovbox.Text
end)
end)