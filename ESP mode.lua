game.StarterGui:SetCore("SendNotification", {
Title = "ESP MODE";
Text = "PRESS - to hide this UI",
Icon = "rbxassetid://9633516381";
Duration = 15;
})
game.StarterGui:SetCore("SendNotification", {
Title = "Choose one";
Text = "Which one you want to using",
Icon = "rbxassetid://9633516381";
Duration = 9;
})

local ScreenGui = Instance.new("ScreenGui")
local Frame = Instance.new("Frame")
local SimpleESP = Instance.new("TextButton")
local Frame_2 = Instance.new("Frame")
local FatesESP = Instance.new("TextButton")
--Properties:

ScreenGui.Parent = game.CoreGui
ScreenGui.ZIndexBehavior = Enum.ZIndexBehavior.Sibling

Frame.Parent = ScreenGui
Frame.BackgroundColor3 = Color3.fromRGB(70, 70, 70)
Frame.BorderSizePixel = 0
Frame.Position = UDim2.new(0.5817941949, 0, 0.031096994, 0)
Frame.Size = UDim2.new(0, 170, 0, 89)
Frame.Draggable = true 
Frame.Active = true 

SimpleESP.Name = "SimpleESP"
SimpleESP.Parent = Frame
SimpleESP.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
SimpleESP.BorderSizePixel = 0
SimpleESP.Position = UDim2.new(0.0180065946, 0, 0.213617444, 0)
SimpleESP.Size = UDim2.new(0, 70, 0, 22)
SimpleESP.Style = Enum.ButtonStyle.RobloxRoundButton
SimpleESP.Font = Enum.Font.SourceSans
SimpleESP.Text = "SimpleESP"
SimpleESP.TextColor3 = Color3.fromRGB(255, 255, 255)
SimpleESP.TextSize = 12.000
SimpleESP.MouseButton1Down:connect(function()
loadstring(game:HttpGet("https://gitlab.com/ozukKZ/pg/-/raw/main/simpleESP.lua"))()
end)
Frame_2.Parent = Frame
Frame_2.BackgroundColor3 = Color3.fromRGB(255, 26, 26)
Frame_2.BorderSizePixel = 0
Frame_2.Size = UDim2.new(0, 170, 0, 7)

FatesESP.Name = "FatesESP"
FatesESP.Parent = Frame
FatesESP.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
FatesESP.BorderSizePixel = 0
FatesESP.Position = UDim2.new(0.544620593, 0, 0.213617444, 0)
FatesESP.Size = UDim2.new(0, 70, 0, 22)
FatesESP.Style = Enum.ButtonStyle.RobloxRoundButton
FatesESP.Font = Enum.Font.SourceSans
FatesESP.Text = "FatesESP"
FatesESP.TextColor3 = Color3.fromRGB(255, 255, 255)
FatesESP.TextSize = 14.000
FatesESP.MouseButton1Down:connect(function()
loadstring(game:HttpGet("https://gitlab.com/ozukKZ/pg/-/raw/main/fatesesp.lua", true))()
end)

--scripts

local Frame = -- GUI
	game:GetService("Players").LocalPlayer:GetMouse().KeyDown:Connect(function(Key)
	if Key == "-" then
		Frame.Visible = not Frame.Visible
	end
end)

