game.StarterGui:SetCore("SendNotification", {
Title = "AIMBOT MODE";
Text = "PRESS - to hide this UI",
Icon = "rbxassetid://5472203252";
Duration = 15;
})
game.StarterGui:SetCore("SendNotification", {
Title = "Choose one";
Text = "Which one you want to using",
Icon = "rbxassetid://5472203252";
Duration = 9;
})

local ScreenGui = Instance.new("ScreenGui")
local Frame = Instance.new("Frame")
local Aimbotsimple = Instance.new("TextButton")
local Aimbotvisuals = Instance.new("TextButton")
local Frame_2 = Instance.new("Frame")
local Aimbotedit = Instance.new("TextButton")
--Properties:

ScreenGui.Parent = game.CoreGui
ScreenGui.ZIndexBehavior = Enum.ZIndexBehavior.Sibling

Frame.Parent = ScreenGui
Frame.BackgroundColor3 = Color3.fromRGB(70, 70, 70)
Frame.BorderSizePixel = 0
Frame.Position = UDim2.new(0.7817941949, 0, 0.031096994, 0)
Frame.Size = UDim2.new(0, 170, 0, 89)
Frame.Draggable = true 
Frame.Active = true 

Aimbotsimple.Name = "Aimbotsimple"
Aimbotsimple.Parent = Frame
Aimbotsimple.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Aimbotsimple.BorderSizePixel = 0
Aimbotsimple.Position = UDim2.new(0.0180065946, 0, 0.213617444, 0)
Aimbotsimple.Size = UDim2.new(0, 70, 0, 22)
Aimbotsimple.Style = Enum.ButtonStyle.RobloxRoundButton
Aimbotsimple.Font = Enum.Font.SourceSans
Aimbotsimple.Text = "Aimbot Simple"
Aimbotsimple.TextColor3 = Color3.fromRGB(255, 255, 255)
Aimbotsimple.TextSize = 12.000
Aimbotsimple.MouseButton1Down:connect(function()
loadstring(game:HttpGet("https://gitlab.com/ozukKZ/pg/-/raw/main/AIMBOT.lua"))()
end)

Aimbotvisuals.Name = "Aimbot Visuals"
Aimbotvisuals.Parent = Frame
Aimbotvisuals.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Aimbotvisuals.BorderSizePixel = 0
Aimbotvisuals.Position = UDim2.new(0.0180065946, 0, 0.513617444, 0)
Aimbotvisuals.Size = UDim2.new(0, 70, 0, 22)
Aimbotvisuals.Style = Enum.ButtonStyle.RobloxRoundButton
Aimbotvisuals.Font = Enum.Font.SourceSans
Aimbotvisuals.Text = "Aimbot Visuals"
Aimbotvisuals.TextColor3 = Color3.fromRGB(255, 255, 255)
Aimbotvisuals.TextSize = 12.000
Aimbotvisuals.MouseButton1Down:connect(function()
loadstring(game:HttpGet("https://gitlab.com/ozukKZ/aimbot/-/raw/main/GUI.lua"))()
end)

Frame_2.Parent = Frame
Frame_2.BackgroundColor3 = Color3.fromRGB(255, 102, 0)
Frame_2.BorderSizePixel = 0
Frame_2.Size = UDim2.new(0, 170, 0, 7)

Aimbotedit.Name = "Aimbotedit"
Aimbotedit.Parent = Frame
Aimbotedit.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Aimbotedit.BorderSizePixel = 0
Aimbotedit.Position = UDim2.new(0.544620593, 0, 0.213617444, 0)
Aimbotedit.Size = UDim2.new(0, 70, 0, 22)
Aimbotedit.Style = Enum.ButtonStyle.RobloxRoundButton
Aimbotedit.Font = Enum.Font.SourceSans
Aimbotedit.Text = "Aimbot Edit"
Aimbotedit.TextColor3 = Color3.fromRGB(255, 255, 255)
Aimbotedit.TextSize = 14.000
Aimbotedit.MouseButton1Down:connect(function()
loadstring(game:HttpGet("https://gitlab.com/ozukKZ/pg/-/raw/main/aimbotEdit.lua", true))()
end)

--scripts

local Frame = -- GUI
	game:GetService("Players").LocalPlayer:GetMouse().KeyDown:Connect(function(Key)
	if Key == "-" then
		Frame.Visible = not Frame.Visible
	end
end)

