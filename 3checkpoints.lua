--KZ
game.StarterGui:SetCore("SendNotification", {
Title = "Position Keyboard!";
Text = "Thank for using! X3",
Icon = "rbxassetid://10678268125";
Duration = 9;
})
spawn(function()--||
local TPOP = Instance.new("ScreenGui")
local main = Instance.new("Frame")
local PT = Instance.new("TextLabel")
local TPTO = Instance.new("Frame")
local PL = Instance.new("TextLabel")


TPOP.Name = "TPOP"
TPOP.Parent = game.CoreGui

main.Name = "main"
main.Parent = TPOP
main.BackgroundColor3 = Color3.new(0.384314, 0.384314, 0.384314)
main.Position = UDim2.new(0.997280121, 0, 0.117114937, 0)--
main.Size = UDim2.new(0, 140, 0, 40)--=

PT.Name = "PT"
PT.Parent = main
PT.BackgroundColor3 = Color3.new(1, 1, 1)
PT.BackgroundTransparency = 1
PT.Position = UDim2.new(-0.00198434386, 0, 0.00943762809, 0)
PT.Size = UDim2.new(0, 140, 0, 40)--=
PT.Font = Enum.Font.SourceSansSemibold
PT.Text = "Position Has Been Saved!"
PT.TextColor3 = Color3.new(0, 255, 255)
PT.TextSize = 15
PT.TextWrapped = true

TPTO.Name = "TPTO"
TPTO.Parent = TPOP
TPTO.BackgroundColor3 = Color3.new(0.384314, 0.384314, 0.384314)
TPTO.Position = UDim2.new(0.997280121, 0, 0.17823962, 0)---
TPTO.Size = UDim2.new(0, 140, 0, 40)--

PL.Name = "PL"
PL.Parent = TPTO
PL.BackgroundColor3 = Color3.new(1, 1, 1)
PL.BackgroundTransparency = 1
PL.Position = UDim2.new(-0.00198434386, 0, 0.00943762809, 0)
PL.Size = UDim2.new(0, 140, 0, 40)--
PL.Font = Enum.Font.SourceSansSemibold
PL.Text = "Position 1 Has Been Loaded!<3"
PL.TextColor3 = Color3.new(1, 0.666667, 0)
PL.TextSize = 16
PL.TextWrapped = true


PT.Text = "Save1, Load2"

wait(1)

main:TweenPosition(UDim2.new(0.911181152, 0, 0.117114937, 0), "Out")--

wait(2)

main:TweenPosition(UDim2.new(0.99728012, 0, 0.117114937, 0), "In")--

wait(1)

PT.Text = "Position 1 Has Been Saved!"


function onKeyPress(inputObject, gameProcessedEvent)
if inputObject.KeyCode == Enum.KeyCode.KeypadOne then --KeypadOne
LPos1 = game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame
main:TweenPosition(UDim2.new(0.911181152, 0, 0.117114937, 0), "Out")--
wait(2)
main:TweenPosition(UDim2.new(0.99728012, 0, 0.117114937, 0), "In")--
end
end
game:GetService("UserInputService").InputBegan:connect(onKeyPress)

function onKeyPress(inputObject, gameProcessedEvent)
if inputObject.KeyCode == Enum.KeyCode.KeypadTwo then --KeypadTwo
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = LPos1
TPTO:TweenPosition(UDim2.new(0.911221445, 0, 0.17823962, 0), "Out")---
wait(2)
TPTO:TweenPosition(UDim2.new(0.99728012, 0, 0.17823962, 0), "In")---
end
end
game:GetService("UserInputService").InputBegan:connect(onKeyPress)
--
end)
-----------------------------------------------------------------------------------
spawn(function()--||
wait(0.5)
local TPOP2 = Instance.new("ScreenGui")
local main2 = Instance.new("Frame")
local PT2 = Instance.new("TextLabel")
local TPTO2 = Instance.new("Frame")
local PL2 = Instance.new("TextLabel")


TPOP2.Name = "TPOP2"
TPOP2.Parent = game.CoreGui

main2.Name = "main2"
main2.Parent = TPOP2
main2.BackgroundColor3 = Color3.new(0.384314, 0.384314, 0.384314)
main2.Position = UDim2.new(0.997280121, 0, 0.317114937, 0)
main2.Size = UDim2.new(0, 140, 0, 40)

PT2.Name = "PT2"
PT2.Parent = main2
PT2.BackgroundColor3 = Color3.new(1, 1, 1)
PT2.BackgroundTransparency = 1
PT2.Position = UDim2.new(-0.00198434386, 0, 0.00943762809, 0)
PT2.Size = UDim2.new(0, 140, 0, 40)
PT2.Font = Enum.Font.SourceSansSemibold
PT2.Text = "Position 2 Has Been Saved!"
PT2.TextColor3 = Color3.new(0, 255, 255)
PT2.TextSize = 15
PT2.TextWrapped = true

TPTO2.Name = "TPTO2"
TPTO2.Parent = TPOP2
TPTO2.BackgroundColor3 = Color3.new(0.384314, 0.384314, 0.384314)
TPTO2.Position = UDim2.new(0.997280121, 0, 0.37823962, 0)
TPTO2.Size = UDim2.new(0, 140, 0, 40)

PL2.Name = "PL2"
PL2.Parent = TPTO2
PL2.BackgroundColor3 = Color3.new(1, 1, 1)
PL2.BackgroundTransparency = 1
PL2.Position = UDim2.new(-0.00198434386, 0, 0.00943762809, 0)
PL2.Size = UDim2.new(0, 140, 0, 40)
PL2.Font = Enum.Font.SourceSansSemibold
PL2.Text = "Position 2 Has Been Loaded!<3"
PL2.TextColor3 = Color3.new(1, 0.666667, 0)
PL2.TextSize = 16
PL2.TextWrapped = true


PT2.Text = "Save4, Load5"

wait(1)

main2:TweenPosition(UDim2.new(0.911181152, 0, 0.317114937, 0), "Out")

wait(2)

main2:TweenPosition(UDim2.new(0.99728012, 0, 0.317114937, 0), "In")

wait(1)

PT2.Text = "Position 2 Has Been Saved!"


function onKeyPress(inputObject, gameProcessedEvent)
if inputObject.KeyCode == Enum.KeyCode.KeypadFour then --KeypadOne
LPos2 = game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame
main2:TweenPosition(UDim2.new(0.911181152, 0, 0.317114937, 0), "Out")
wait(2)
main2:TweenPosition(UDim2.new(0.99728012, 0, 0.317114937, 0), "In")
end
end
game:GetService("UserInputService").InputBegan:connect(onKeyPress)

function onKeyPress(inputObject, gameProcessedEvent)
if inputObject.KeyCode == Enum.KeyCode.KeypadFive then --KeypadTwo
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = LPos2
TPTO2:TweenPosition(UDim2.new(0.911221445, 0, 0.37823962, 0), "Out")
wait(2)
TPTO2:TweenPosition(UDim2.new(0.99728012, 0, 0.37823962, 0), "In")
end
end
game:GetService("UserInputService").InputBegan:connect(onKeyPress)
--
end)
-------------------------------------------------------------------------------
spawn(function()--||
wait(1)
local TPOP3 = Instance.new("ScreenGui")
local main3 = Instance.new("Frame")
local PT3 = Instance.new("TextLabel")
local TPTO3 = Instance.new("Frame")
local PL3 = Instance.new("TextLabel")


TPOP3.Name = "TPOP3"
TPOP3.Parent = game.CoreGui

main3.Name = "main3"
main3.Parent = TPOP3
main3.BackgroundColor3 = Color3.new(0.384314, 0.384314, 0.384314)
main3.Position = UDim2.new(0.997280121, 0, 0.517114937, 0)
main3.Size = UDim2.new(0, 140, 0, 40)

PT3.Name = "PT3"
PT3.Parent = main3
PT3.BackgroundColor3 = Color3.new(1, 1, 1)
PT3.BackgroundTransparency = 1
PT3.Position = UDim2.new(-0.00198434386, 0, 0.00943762809, 0)
PT3.Size = UDim2.new(0, 140, 0, 40)
PT3.Font = Enum.Font.SourceSansSemibold
PT3.Text = "Position 3 Has Been Saved!"
PT3.TextColor3 = Color3.new(0, 255, 255)
PT3.TextSize = 15
PT3.TextWrapped = true

TPTO3.Name = "TPTO3"
TPTO3.Parent = TPOP3
TPTO3.BackgroundColor3 = Color3.new(0.384314, 0.384314, 0.384314)
TPTO3.Position = UDim2.new(0.997280121, 0, 0.57823962, 0)
TPTO3.Size = UDim2.new(0, 140, 0, 40)

PL3.Name = "PL3"
PL3.Parent = TPTO3
PL3.BackgroundColor3 = Color3.new(1, 1, 1)
PL3.BackgroundTransparency = 1
PL3.Position = UDim2.new(-0.00198434386, 0, 0.00943762809, 0)
PL3.Size = UDim2.new(0, 140, 0, 40)
PL3.Font = Enum.Font.SourceSansSemibold
PL3.Text = "Position 3 Has Been Loaded!<3"
PL3.TextColor3 = Color3.new(1, 0.666667, 0)
PL3.TextSize = 16
PL3.TextWrapped = true


PT3.Text = "Save7, Load8"

wait(1)

main3:TweenPosition(UDim2.new(0.911181152, 0, 0.517114937, 0), "Out")

wait(2)

main3:TweenPosition(UDim2.new(0.99728012, 0, 0.517114937, 0), "In")

wait(1)

PT3.Text = "Position 3 Has Been Saved!"


function onKeyPress(inputObject, gameProcessedEvent)
if inputObject.KeyCode == Enum.KeyCode.KeypadSeven then --KeypadOne
LPos3 = game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame
main3:TweenPosition(UDim2.new(0.911181152, 0, 0.517114937, 0), "Out")
wait(2)
main3:TweenPosition(UDim2.new(0.99728012, 0, 0.517114937, 0), "In")
end
end
game:GetService("UserInputService").InputBegan:connect(onKeyPress)

function onKeyPress(inputObject, gameProcessedEvent)
if inputObject.KeyCode == Enum.KeyCode.KeypadEight then --KeypadTwo
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = LPos3
TPTO3:TweenPosition(UDim2.new(0.911221445, 0, 0.57823962, 0), "Out")
wait(2)
TPTO3:TweenPosition(UDim2.new(0.99728012, 0, 0.57823962, 0), "In")
end
end
game:GetService("UserInputService").InputBegan:connect(onKeyPress)
--
end)
