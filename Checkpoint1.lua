--KZ
spawn(function()
game.StarterGui:SetCore("SendNotification", {
Title = "Checkpoint!1";
Text = "1 save,2 tp",
Icon = "rbxassetid://5472203252";
Duration = 9;
})
local TPOP = Instance.new("ScreenGui")
local main = Instance.new("Frame")
local PT = Instance.new("TextLabel")
local TPTO = Instance.new("Frame")
local PL = Instance.new("TextLabel")


TPOP.Name = "TPOP"
TPOP.Parent = game.CoreGui

main.Name = "main"
main.Parent = TPOP
main.BackgroundColor3 = Color3.new(0.384314, 0.384314, 0.384314)
main.Position = UDim2.new(0.997280121, 0, 0.117114937, 0)--
main.Size = UDim2.new(0, 140, 0, 40)--=

PT.Name = "PT"
PT.Parent = main
PT.BackgroundColor3 = Color3.new(1, 1, 1)
PT.BackgroundTransparency = 1
PT.Position = UDim2.new(-0.00198434386, 0, 0.00943762809, 0)
PT.Size = UDim2.new(0, 140, 0, 40)--=
PT.Font = Enum.Font.SourceSansSemibold
PT.Text = "Position Has Been Saved!"
PT.TextColor3 = Color3.new(0, 255, 255)
PT.TextSize = 15
PT.TextWrapped = true

TPTO.Name = "TPTO"
TPTO.Parent = TPOP
TPTO.BackgroundColor3 = Color3.new(0.384314, 0.384314, 0.384314)
TPTO.Position = UDim2.new(0.997280121, 0, 0.17823962, 0)---
TPTO.Size = UDim2.new(0, 140, 0, 40)--

PL.Name = "PL"
PL.Parent = TPTO
PL.BackgroundColor3 = Color3.new(1, 1, 1)
PL.BackgroundTransparency = 1
PL.Position = UDim2.new(-0.00198434386, 0, 0.00943762809, 0)
PL.Size = UDim2.new(0, 140, 0, 40)--
PL.Font = Enum.Font.SourceSansSemibold
PL.Text = "Position 1 Has Been Loaded!<3"
PL.TextColor3 = Color3.new(1, 0.666667, 0)
PL.TextSize = 16
PL.TextWrapped = true


PT.Text = "Save1, Load2"

wait(1)

main:TweenPosition(UDim2.new(0.911181152, 0, 0.117114937, 0), "Out")--

wait(2)

main:TweenPosition(UDim2.new(0.99728012, 0, 0.117114937, 0), "In")--

wait(1)

PT.Text = "Position 1 Has Been Saved!"


function onKeyPress(inputObject, gameProcessedEvent)
if inputObject.KeyCode == Enum.KeyCode.KeypadOne then --KeypadOne
LPos1 = game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame
main:TweenPosition(UDim2.new(0.911181152, 0, 0.117114937, 0), "Out")--
wait(2)
main:TweenPosition(UDim2.new(0.99728012, 0, 0.117114937, 0), "In")--
end
end
game:GetService("UserInputService").InputBegan:connect(onKeyPress)

function onKeyPress(inputObject, gameProcessedEvent)
if inputObject.KeyCode == Enum.KeyCode.KeypadTwo then --KeypadTwo
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = LPos1
TPTO:TweenPosition(UDim2.new(0.911221445, 0, 0.17823962, 0), "Out")---
wait(2)
TPTO:TweenPosition(UDim2.new(0.99728012, 0, 0.17823962, 0), "In")---
end
end
game:GetService("UserInputService").InputBegan:connect(onKeyPress)
--
end)
