spawn(function()
game.StarterGui:SetCore("SendNotification", {
Title = "Checkpoint!2";
Text = "4 save,5 tp",
Icon = "rbxassetid://5472203252";
Duration = 9;
})
local TPOP2 = Instance.new("ScreenGui")
local main2 = Instance.new("Frame")
local PT2 = Instance.new("TextLabel")
local TPTO2 = Instance.new("Frame")
local PL2 = Instance.new("TextLabel")


TPOP2.Name = "TPOP2"
TPOP2.Parent = game.CoreGui

main2.Name = "main2"
main2.Parent = TPOP2
main2.BackgroundColor3 = Color3.new(0.384314, 0.384314, 0.384314)
main2.Position = UDim2.new(0.997280121, 0, 0.317114937, 0)
main2.Size = UDim2.new(0, 140, 0, 40)

PT2.Name = "PT2"
PT2.Parent = main2
PT2.BackgroundColor3 = Color3.new(1, 1, 1)
PT2.BackgroundTransparency = 1
PT2.Position = UDim2.new(-0.00198434386, 0, 0.00943762809, 0)
PT2.Size = UDim2.new(0, 140, 0, 40)
PT2.Font = Enum.Font.SourceSansSemibold
PT2.Text = "Position 2 Has Been Saved!"
PT2.TextColor3 = Color3.new(0, 255, 255)
PT2.TextSize = 15
PT2.TextWrapped = true

TPTO2.Name = "TPTO2"
TPTO2.Parent = TPOP2
TPTO2.BackgroundColor3 = Color3.new(0.384314, 0.384314, 0.384314)
TPTO2.Position = UDim2.new(0.997280121, 0, 0.37823962, 0)
TPTO2.Size = UDim2.new(0, 140, 0, 40)

PL2.Name = "PL2"
PL2.Parent = TPTO2
PL2.BackgroundColor3 = Color3.new(1, 1, 1)
PL2.BackgroundTransparency = 1
PL2.Position = UDim2.new(-0.00198434386, 0, 0.00943762809, 0)
PL2.Size = UDim2.new(0, 140, 0, 40)
PL2.Font = Enum.Font.SourceSansSemibold
PL2.Text = "Position 2 Has Been Loaded!<3"
PL2.TextColor3 = Color3.new(1, 0.666667, 0)
PL2.TextSize = 16
PL2.TextWrapped = true


PT2.Text = "Save4, Load5"

wait(1)

main2:TweenPosition(UDim2.new(0.911181152, 0, 0.317114937, 0), "Out")

wait(2)

main2:TweenPosition(UDim2.new(0.99728012, 0, 0.317114937, 0), "In")

wait(1)

PT2.Text = "Position 2 Has Been Saved!"


function onKeyPress2(inputObject, gameProcessedEvent2)
if inputObject.KeyCode == Enum.KeyCode.KeypadFour then --KeypadOne
LPos2 = game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame
main2:TweenPosition(UDim2.new(0.911181152, 0, 0.317114937, 0), "Out")
wait(2)
main2:TweenPosition(UDim2.new(0.99728012, 0, 0.317114937, 0), "In")
end
end
game:GetService("UserInputService").InputBegan:connect(onKeyPress2)

function onKeyPress2(inputObject, gameProcessedEvent2)
if inputObject.KeyCode == Enum.KeyCode.KeypadFive then --KeypadTwo
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = LPos2
TPTO2:TweenPosition(UDim2.new(0.911221445, 0, 0.37823962, 0), "Out")
wait(2)
TPTO2:TweenPosition(UDim2.new(0.99728012, 0, 0.37823962, 0), "In")
end
end
game:GetService("UserInputService").InputBegan:connect(onKeyPress2)
--
end)
